import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './base.signup.component.html',
  styleUrls: ['./base.signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  bgImg;
  isSubmitted = false;
  orderby: any;
  parm: any;
  msg: any;
  constructor(private route: ActivatedRoute, private router: Router,private _auth: AuthService, private fb: FormBuilder, private _ds:DataService, private http: HttpClient) { }

  ngOnInit(): void {
  //   this.route.queryParams.subscribe(params => {
  //     let date = params;
  //     console.log(date); // Print the parameter to the console. 
  // });
  this.route.queryParams
  .subscribe(params => {
   // console.log(params); // { orderby: "price" }
    this.orderby = params.orderby;
   // console.log(this.orderby); // price
    this.parm = params;
    console.log(this.parm.utm_medium)
    }
  );
    this.signupForm =  this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      city: [''],
      mrn: ['', Validators.required],
      terms: ['', Validators.required]
    });
//     this._ds.getSettingSection().subscribe(res=>
//     document.getElementById("bg").style.backgroundImage = "url("+'background.jpg'+")"
//     // console.log(res['bgImages']['signup'])
// );
  }
  submitSignup(data) {
    console.log('erespone', this.signupForm.value)
    this.isSubmitted = true;
    const formData = new FormData();
    formData.append('name', this.signupForm.value.name);
    formData.append('email', this.signupForm.value.email);
    formData.append('company', this.signupForm.value.mrn);
    if(this.parm.utm_medium==undefined){
      formData.append('designation', 'other');
    }else{
    formData.append('designation', this.parm.utm_medium);
    }
    formData.append('mobile', this.signupForm.value.mobile);
    if(this.signupForm.value.city==''){
      formData.append('category', "others");
    }else{
    formData.append('category', this.signupForm.value.city);
    }
    if (this.signupForm.valid) {
      this._auth.signupMethod(formData).subscribe((res: any) => {
        if (res.code == 1) {
          this.isSubmitted = false;
          // Swal.fire({
          //   position: 'center',
          //   icon: 'success',
          //   title: 'Registration successful',
          //   text:"Join us for a refreshing exchange on epidemiology and immunology, vaccine trials, development of new vaccines and much more. Lets debate and discuss further on 12th & 13th March 2022 | 3.00 PM - 6.30 PM",
          //   showConfirmButton: false,
          //   timer: 3000
          // });
          // Swal.fire('Registration successful')
          Swal.fire(
            
            'Registration successful',
            'Join us for a refreshing exchange on epidemiology and immunology, vaccine trials, development of new vaccines and much more. Lets debate and discuss further on 12th & 13th March 2022 | 3.00 PM - 6.30 PM',
            
          )
          
          data.resetForm(); 
          this.router.navigate(['/login']);
          // this.resetForm();
        } else {      
          this.msg=res.result      
          data.resetForm();          
          // this.resetForm();
        }
        
      });
    } else {
      console.log("thisis msg")
    }
  }
  // resetForm(){
  //   this.signupForm.patchValue({
  //     mobile: '***',
  //     city: '***',
  //     gender: 'male',
  //     is_founded: '**',
  //     policy: '**'
  //   });
  // }
}
