import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { Eventid } from '../shared/eventid';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  event_id = Eventid.event_id;
  isVideoPlayed = false;
  menu_type;
  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router,private _fd: DataService) { }

  ngOnInit(): void {
   
   /*  data = JSON.parse(localStorage.getItem('myKey'));
    console.log(data.name) */


    localStorage.setItem('tour_guide', 'start_guide');


    this.loginForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      // terms: ['', Validators.required]
    });
  }
  skip(){
    this.router.navigate(['/lobby']);
  }
  submitLogin(){
    const user = {
      email: this.loginForm.value.email,
      event_id: this.event_id,
      role_id: 1,
    };

    const formData = new FormData();
    formData.append('name', this.loginForm.value.name);
    formData.append('email', this.loginForm.value.email);
    formData.append('mobile', 'others');
    formData.append('category', 'others');
    formData.append('company', 'others');
    formData.append('designation', 'others');

//     name:Divakar
// email:divakar.kumar6@gmail.comkkkoioik
// mobile:9899785515
// category:Male
// company:Bihar
// designation:30
    // console.log(user);
    var isMobile = {
      Android: ()=> {
          return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: ()=> {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: ()=> {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: ()=> {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: ()=> {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: ()=> {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
    };
  
    this.auth.loginMethod(formData).subscribe((res: any) => {
      if (res.code === 1) {
        localStorage.setItem('menu_type', JSON.stringify(res.result.menu_type));
        let emails = this.loginForm.value.email.toLowerCase();

        this._fd.signupRaise(emails).subscribe((res:any) => {

          });
        // this._fd.signupRaise(emails).subscribe((res:any) => {

        // });
          this._fd.getloginforhand(emails).subscribe((res: any) => {
            localStorage.setItem('getdata', JSON.stringify(res.result));
          })
          localStorage.setItem('myemail', emails);

        // if( isMobile.iOS() ){
        //   this.isVideoPlayed = false;
        //   this.router.navigateByUrl('/lobby');
        // }
        // if(res.result.outer_animation == true){
          // let vid: any = document.getElementById('outerVideo');
          // let sources = vid.getElementsByTagName('source');
          // sources[0].src = res.result.outer_animation_url;
          // vid.load();
          // vid.play();
        this.router.navigateByUrl('/animate');
        // }
        localStorage.setItem('virtual', JSON.stringify(res.result));
        this.isVideoPlayed = false; 
        
      } else {
        this.isVideoPlayed = false;
        this.loginForm.reset();
      }
    }, (err: any) => {
      this.isVideoPlayed = false;
      console.log('error', err)
    });
  }
  endVideo(){
    this.router.navigate(['/lobby']);
  }
}
