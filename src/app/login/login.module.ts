import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnimateComponent } from './animate/animate.component';
import { DirectloginComponent } from './directlogin/directlogin.component';


@NgModule({
  declarations: [LoginComponent, AnimateComponent, DirectloginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LoginModule { }
