import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var introJs: any;

@Component({
  selector: 'app-directlogin',
  templateUrl: './directlogin.component.html',
  styleUrls: ['./directlogin.component.scss']
})
export class DirectloginComponent implements OnInit {
  uid: any;
  intro: any;
  videoPlay = false;
  registrationDesk = false;
  yesSkip=false;
  constructor(private router: Router,private auth: AuthService,private fb: FormBuilder) { }

  ngOnInit(): void {
    // this.getUserguide();
    var navigator_info = window.navigator;
    var screen_info = window.screen;
    this.uid = navigator_info.mimeTypes.length;
    this.uid += navigator_info.userAgent.replace(/\D+/g, '');
    this.uid += navigator_info.plugins.length;
    this.uid += screen_info.height || '';
    this.uid += screen_info.width || '';
    this.uid += screen_info.pixelDepth || '';

    // alert(this.uid)
    const formData = new FormData();
    formData.append('name', this.uid);
    formData.append('email', this.uid+'@sanofi.com');
    formData.append('mobile', 'others');
    formData.append('category', 'others');
    formData.append('company', 'others');
    formData.append('designation', 'others');

    this.auth.loginMethod(formData).subscribe((res: any) => {
      if (res.code === 1) {
        // alert('Successfully')
        localStorage.setItem('virtual', JSON.stringify(res.result));

      }
    });
  };

  endVideo(){
    this.router.navigate(['/lobby']);
  }
  skip(){
    let vid: any = document.getElementById('loginVideo');
    vid.pause();
    vid.currentActiveTime = 0;
    this.router.navigate(['/lobby']);
  }

  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      showBullets: false,
      showStepNumbers: false,
      steps: [
        {
          element: document.querySelector("#reception_pulse"),
          intro: "<div style='text-align:center'>Click here to visit the lobby</div>",
          position: 'right',
        }
      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }

  videoplay(){

    //   this.videoPlay = true;
    //         let vid: any = document.getElementById('myVideo');
    //         vid.play();
  // this.receptionEnd = true;
      // this.showVideo = true;
      // let vid: any = document.getElementById("recepVideo");
      // vid.play();
    // vid.currentActiveTime = 0;
    this.registrationDesk = true;
    this.yesSkip=true;
    this.videoPlay = true;
    let vid: any = document.getElementById("loginVideo");
    // window.onload=function(){
      vid.play();
    }

}
