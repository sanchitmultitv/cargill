import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './@main/layout.component';
import { AnimateComponent } from './login/animate/animate.component';
import { DirectloginComponent } from './login/directlogin/directlogin.component';
import { MaincallComponent } from './maincall/maincall.component';

const routes: Routes = [
  { path:'', redirectTo:'login', pathMatch:'prefix'},

  { path:'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'maincall', component:MaincallComponent},
  { path: 'animate', component:AnimateComponent},
  { path: 'directLogin', component:DirectloginComponent},
  // { path: 'directlogin', component:DirectloginComponent},
  {
    path: '',
    component: LayoutComponent,
    children: [
        { path: '', loadChildren: ()=>import('./@main/layout.module').then(m=>m.LayoutModule)}
      ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
